import * as React from "react";
import "./App.css";
//import { Constants } from './constants/index';
//import { Types } from './types/index';
import { any } from "prop-types";
import { types } from "@babel/core";

enum Player {
  None = 'null',
  One = 1,
  Two = 2
}

enum GameState {
  Ongoing = -1,
  Draw = 0,
  PlayerOneWin = Player.One,
  PlayerTwoWin = Player.Two
}
type Board = Player[];

interface State {
  board: Board;
  playerTurn: Player;
  gameState: GameState | Player
}

const intitializeBoard = () => {
  const board = [];
  for(let i = 0; i < 42; i++) {
    board.push(Player.None);
  }
  return board;
};

const getPlayer = (player:Player) => {
  if (player === Player.None)
      return "noPlayer";
  if (player === Player.One)
      return "playerOne";
  if (player === Player.Two)
      return "playerTwo";
};

const findLowestEmptyIndex = (board: Board, column: number) => {
  for (let i = 35 + column; i >= 0; i -= 7) {
    if (board[i] === Player.None) return i;
  }

  return -1;
};

const togglePlayerTurn = (player: Player) => {
  return player === Player.One ? Player.Two : Player.One;
};

const getGameState = (board: Board) => {
  // Checks wins horizontally
  for (let x = 0; x < 6; x++) {
    for (let z = 0; z <= 4; z++) {
      const index = x * 7 + z;
      const boardSlice = board.slice(index, index + 4);

      const winningResult = checkWinningSlice(boardSlice);
      if (winningResult !== false)
         return winningResult;
    }
  }

  // check wins vertically
  for (let x = 0; x <= 2; x++) {
    for (let z = 0; z < 7; z++) {
      const index = x * 7 + z;
      const boardSlice = [
        board[index],
        board[index + 7],
        board[index + 7 * 2],
        board[index + 7 * 3]
      ];

      const winningResult = checkWinningSlice(boardSlice);
      if (winningResult !== false)
         return winningResult;
    }
  }

  // check wins diagonally
  for (let x = 0; x <= 2; x++) {
    for (let z = 0; z < 7; z++) {
      const index = x * 7 + z;

      // Checks diagonal down-left
      if (z >= 3) {
        const boardSlice = [
          board[index],
          board[index + 7 - 1],
          board[index + 7 * 2 - 2],
          board[index + 7 * 3 - 3]
        ];
  
        const winningResult = checkWinningSlice(boardSlice);
        if (winningResult !== false)
           return winningResult;
      } 

      // Checks diagonal down-right
      if (z <= 3) {
        const boardSlice = [
          board[index],
          board[index + 7 + 1],
          board[index + 7 * 2 + 2],
          board[index + 7 * 3 + 3]
        ];
  
        const winningResult = checkWinningSlice(boardSlice);
        if (winningResult !== false)
           return winningResult;
      }
    }
  }

  if (board.some(cell => cell === Player.None)) {
    return GameState.Ongoing
  } else {
    return GameState.Draw
  }
};

const checkWinningSlice = (miniBoard: Player[]) => {
  if (miniBoard.some(cell => cell === Player.None))
     return false;
  if (miniBoard[0] === miniBoard[1] && miniBoard[1] === miniBoard[2] && miniBoard[2] === miniBoard[3]){
    return miniBoard[1];
  }
  return false;
};

class App extends React.Component<{}, State> {
  state: State = {
    board: intitializeBoard(),
    playerTurn: Player.One,
    gameState: GameState.Ongoing
  };

  public renderCells = () => {
    const { board } = this.state;
    return board.map((player:any, index:any) => this.renderCell(player, index));
  };

  public handleOnClick = (index: number) => () => {
    const {gameState} = this.state

    if(gameState !== GameState.Ongoing) 
       return 
    
    const column = index % 7;
    this.makeMove(column);
  };

  public makeMove(column: number) {
    const { board, playerTurn } = this.state;

    const index = findLowestEmptyIndex(board, column);

    const newBoard = board.slice();
    newBoard[index] = playerTurn;

    const gameState = getGameState(newBoard);

    this.setState({
      board: newBoard,
      playerTurn: togglePlayerTurn(playerTurn),
      gameState
    });
  }

  public renderCell = (player:Player, index: number) => {
    return (
      <div className="cell" key={index}
        onClick={this.handleOnClick(index)}
        data-player={getPlayer(player)}
      />
    );
  };

  public renderGameStatus = () => {
    const { gameState } = this.state 
    let text
    if (gameState === GameState.Ongoing) {
      text = 'Game is ongoing';
    } else if (gameState === GameState.Draw) {
      text = 'Game is a draw';
    } else if (gameState === GameState.PlayerOneWin) {
      text = 'Player one won';
    } else if (gameState === GameState.PlayerTwoWin) {
      text = 'Player two won';
    }
    return <div>
      {text}
    </div>
  }

  public render() {
    return (
      <div className="App">
       <h1>{this.renderGameStatus() }</h1> 
        <div className="game-board">
        {this.renderCells()}
        </div>
      </div>
    );
  }
}

export default App;
